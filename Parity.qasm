OPENQASM 2.0;
include "qelib1.inc";

gate O_Parity io_in_0,io_in_1,io_in_2,io_in_3,io_out {
        ancilla anc[1];
        cx io_in_2,anc[0];
        cx io_in_3,anc[0];
        cx io_in_0,io_out;
        cx io_in_1,io_out;
        cx anc[0],io_out;
        cx io_in_2,anc[0];
        cx io_in_3,anc[0];
}