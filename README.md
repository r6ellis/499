The test scripts for classical designs are generally all under the test branch. (accessed via sbt test on command line)

Running the QLib is currently only supported for small designs (2 inputs, lol), and the main app to do so is made in hamming.scala, which can be run via sbt run.

In general to run project, git clone and let metals set itself up, then sbt test or run as you want.