package FIR
// using scala "2.13.12"
// using dep "org.chipsalliance::chisel::5.1.0"
// using plugin "org.chipsalliance:::chisel-plugin::5.1.0"
// using options "-unchecked", "-deprecation", "-language:reflectiveCalls", "-feature", "-Xcheckinit", "-Xfatal-warnings", "-Ywarn-dead-code", "-Ywarn-unused", "-Ymacro-annotations"
//val path = System.getProperty("user.dir") + "/source/load-ivy.sc"
//interp.load.module(ammonite.ops.Path(java.nio.file.FileSystems.getDefault().getPath(path)))

import chisel3._
//import chisel3.util._
import qlib._
import circt.stage.ChiselStage
import collection._
//import chisel3.tester._
//import chisel3.tester.RawTester.test

import Hamming_Syndrome._


class FIR(in_w: Int, out_w: Int, consts: Seq[SInt]) extends Module {
    val io = IO(new Bundle {
        val in = Input(SInt(in_w.W))
        val out = Output(SInt(out_w.W))
    })
    
    //basic input value storage array
    val regs = mutable.ArrayBuffer[SInt]() //of unset width
    for (i <- 0 until consts.length) {
        if (i == 0) regs += io.in
        else regs += RegNext(regs(i - 1), 0.S)
    }

    io.out := (0 until consts.length).map((i:Int) => regs(i) * consts(i)).reduce(_ +& _)
}

object AlsoNotMain extends App {
    //val path = System.getProperty("user.dir") + "/source/load-ivy.sc"
    //interp.load.module(ammonite.ops.Path(java.nio.file.FileSystems.getDefault().getPath(path)))
    println(ChiselStage.emitSystemVerilogFile(
        gen = new FIR(4, 8, Seq(5.S,6.S,7.S,9.S)),
        firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info")
        ))

    // println(ChiselStage.emitSystemVerilog(
    //     gen = new Parity(8),
    //     firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info")
    // ))
}
