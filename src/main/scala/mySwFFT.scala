package mySwFFT

import breeze.math.{Complex => breeze_Complex}
import breeze.linalg.DenseVector
import breeze.numerics.log2

object SW_FFT_wrapper{
    
    def SW_FFT(input: Array[breeze_Complex]): Array[breeze_Complex] = {
        val depth = log2(input.length).toInt
        if(depth == 1){
            return(input)
        } else{//the rounding on this is SCUFFED
            val temp_even = SW_FFT(input.grouped(2).map(_.head).map((i:breeze_Complex) => (new breeze_Complex(((i.real * (1<<30)).round.toInt)/(1<<30).toDouble, ((i.imag * (1<<30)).round.toInt)/(1<<30).toDouble))).toArray)//.groupBy(2).map(_.head).toList)
            val temp_odd = SW_FFT(input.drop(1).grouped(2).map(_.head).map((i:breeze_Complex) => (new breeze_Complex(((i.real * (1<<30)).round.toInt)/(1<<30).toDouble, ((i.imag * (1<<30)).round.toInt)/(1<<30).toDouble))).toArray)//.groupBy(2).map(_.head).toList)
            var output = input//copy shape
            for (i <- 0 until (1<<depth)/2){
                output(2*i) = temp_even(i)
                output(2*i + 1) = temp_odd(i)
            }
            for (i <- 0 until (1<<depth)/2){
                val twid = new breeze_Complex(math.cos(2*math.Pi*i/(1<<depth)), math.sin(2*math.Pi*i/(1<<depth)))
                val mult = temp_odd(i)*twid
                output(i) = temp_even(i) +  mult
                output(i  + (1<<depth)/2) = temp_odd(i) - mult
            }
            return(output)
        }
    }
}

object FFT_SW extends App{
    val log2N = 5
    val in_w = 32
    val out_w = 32
    //var waveform = Vec(1 << log2N, new Complex(SInt(in_w.W), SInt(in_w.W)))
    var sw_array = Array[breeze_Complex]()        

    for (i <- 0 until 1 << log2N){ //this should put in a cos wave with a frequency we can see??
        //waveform(i).real = (math.cos(2*math.Pi/6 * i)*(1<<31 - 1)).toInt.S(in_w.W)
        //waveform(i).im = 0.S(in_w.W)
        sw_array = sw_array :+ new breeze_Complex(math.cos(2*math.Pi/6 * i), 0)//*(1<<30-1)
    }

    //print(waveform(0).real)

    //val sw_waveform = DenseVector(sw_array)

    val sw_fft_val = mySwFFT.SW_FFT_wrapper.SW_FFT(sw_array)
    sw_fft_val.foreach(print(_))
}