package Hamming_Syndrome
// using scala "2.13.12"
// using dep "org.chipsalliance::chisel::5.1.0"
// using plugin "org.chipsalliance:::chisel-plugin::5.1.0"
// using options "-unchecked", "-deprecation", "-language:reflectiveCalls", "-feature", "-Xcheckinit", "-Xfatal-warnings", "-Ywarn-dead-code", "-Ywarn-unused", "-Ymacro-annotations"
//val path = System.getProperty("user.dir") + "/source/load-ivy.sc"
//interp.load.module(ammonite.ops.Path(java.nio.file.FileSystems.getDefault().getPath(path)))

import chisel3._
//import chisel3.util._
import qlib._
import circt.stage.ChiselStage
import collection._
//import chisel3.tester._
//import chisel3.tester.RawTester.test

import FIR._

class Parity(bits_in: Int) extends QModule {
    val io = IO(new Bundle {
        val in = Input(Vec(bits_in, UInt(1.W)))
        val out = Output(UInt(1.W))
    })
    
    //this cirucit should be quantum synthesizeable
    io.out := io.in.reduce(_ ^ _)

}

class MuxedAdder extends QModule {
    val io = IO(new Bundle{
        val in_add_0 = Input(UInt(2.W))
        val in_add_1 = Input(UInt(2.W))
        val add_out = Output(UInt(3.W))
        val mux_sel = Input(UInt(1.W))
        val out = Output(UInt(3.W))
    })

    io.add_out := io.in_add_0 + io.in_add_1
    io.out := Mux(io.mux_sel.asBool, io.add_out, 2.U(3.W))
}

object qmain extends App {
    //val path = System.getProperty("user.dir") + "/source/load-ivy.sc"
    //interp.load.module(ammonite.ops.Path(java.nio.file.FileSystems.getDefault().getPath(path)))
    // println(ChiselStage.emitSystemVerilogFile(
    //     gen = new Parity(8),//, 32, Seq(2.S, 4.S, 8.S, 2.S)),
    //     firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info")//, "--verilog", "-o", "Parity.v")//, "-o", "Parity.sv")
    //     ))
    qlib.QasmWriter.emitQASMFile(new Parity(2), "Parity")
}
