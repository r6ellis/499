package FFT
import chisel3.Bundle
// // using scala "2.13.12"
// // using dep "org.chipsalliance::chisel::5.1.0"
// // using plugin "org.chipsalliance:::chisel-plugin::5.1.0"
// // using options "-unchecked", "-deprecation", "-language:reflectiveCalls", "-feature", "-Xcheckinit", "-Xfatal-warnings", "-Ywarn-dead-code", "-Ywarn-unused", "-Ymacro-annotations"
// //val path = System.getProperty("user.dir") + "/source/load-ivy.sc"
// //interp.load.module(ammonite.ops.Path(java.nio.file.FileSystems.getDefault().getPath(path)))

import chisel3._
//import chisel3.util._
import circt.stage.ChiselStage
import collection._
import chisel3.experimental._
//import dsptools._ nvm dsptools doesn't exist on the berkley repo anymore....?
// //import chisel3.tester._
// //import chisel3.tester.RawTester.test


class Complex(real_in: SInt, im_in: SInt) extends Bundle{
    var real = real_in
    var im = im_in
}

class FFT(in_w: Int, out_w: Int, log2N: Int) extends Module {
    val io = IO(new Bundle {
        val in = Input(Vec(1<<log2N, new Complex(SInt(in_w.W), SInt(in_w.W))))
        val out = Output(Vec(1<<log2N, new Complex(SInt(in_w.W), SInt(in_w.W))))    
    })

    if(log2N <= 1){io.out := io.in}
    else{
        // do the recursive stuff
        val even = Module(new FFT(in_w, out_w, log2N-1))
        even.io.in := io.in.grouped(2).map(_.head).toList // why is scala the way it is
        val odd = Module(new FFT(in_w, out_w, log2N-1))
        odd.io.in := io.in.drop(1).grouped(2).map(_.head).toList

        val twiddles_re = VecInit((0 until 1 << log2N).map((i:Int) => (math.cos(2*math.Pi*i/(1<<log2N))*((1<<(in_w-2))))).map(_.round.toInt.S(in_w.W))) // scale to width
        val twiddles_im = VecInit((0 until 1 << log2N).map((i:Int) => (math.sin(2*math.Pi*i/(1<<log2N))*((1<<(in_w-2))))).map(_.round.toInt.S(in_w.W))) 

        for (i <- 0 until (1 << (log2N))/2){
            val mult = new Complex(
                (odd.io.out(i).real * twiddles_re(i) - odd.io.out(i).im * twiddles_im(i)),//>>(in_w-2).U, 
                (odd.io.out(i).real * twiddles_im(i) + odd.io.out(i).im * twiddles_re(i))//>>(in_w-2).U
                )

            mult.real = Mux(((mult.real&0x7fff_ffff.S) > 0x4000_0000.S), (mult.real >> (in_w-2).U) + Mux(mult.real>=0.S, 1.S, -1.S), (mult.real >> (in_w-2).U))//bit rounding and stuff
            mult.im = Mux(((mult.im&0x7fff_ffff.S) > 0x4000_0000.S), (mult.im >> (in_w-2).U) + Mux(mult.im>=0.S, 1.S, -1.S), (mult.im >> (in_w-2).U))

            //printf("MULTS: %x %x \n", mult.real, mult.im)
            //printf("IO_INs: %x %x \n", odd.io.out(i).real, odd.io.out(i).im)
            //printf("IO_INs_E: %x %x \n", even.io.out(i).real, even.io.out(i).im)

            io.out(i).real := even.io.out(i).real + mult.real
            io.out(i).im := even.io.out(i).im + mult.im

            io.out(i + (1<<log2N)/2).real := odd.io.out(i).real - mult.real
            io.out(i + (1<<log2N)/2).im := odd.io.out(i).im - mult.im
        }
    }
}


object FFTMain extends App {
    //val path = System.getProperty("user.dir") + "/source/load-ivy.sc"
    //interp.load.module(ammonite.ops.Path(java.nio.file.FileSystems.getDefault().getPath(path)))
    println(ChiselStage.emitSystemVerilog(
        gen = new FFT(32, 32, 4),
        firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info")
        ))
}
