package FSM
// using scala "2.13.12"
// using dep "org.chipsalliance::chisel::5.1.0"
// using plugin "org.chipsalliance:::chisel-plugin::5.1.0"
// using options "-unchecked", "-deprecation", "-language:reflectiveCalls", "-feature", "-Xcheckinit", "-Xfatal-warnings", "-Ywarn-dead-code", "-Ywarn-unused", "-Ymacro-annotations"
//val path = System.getProperty("user.dir") + "/source/load-ivy.sc"
//interp.load.module(ammonite.ops.Path(java.nio.file.FileSystems.getDefault().getPath(path)))

import chisel3._
import circt.stage.ChiselStage
import chisel3.util._
// import collection._
//import chisel3.tester._
//import chisel3.tester.RawTester.test

object ChiselIsDumb {
    object State extends ChiselEnum{
        val rest, init, run, done = Value 
    }
}

class FSM(num_ins: Int) extends Module {
    import ChiselIsDumb.State
    import ChiselIsDumb.State._
    val io = IO(new Bundle {
        val in = Input(Vec(num_ins, UInt(8.W)))
        val out_val = Output(UInt(8.W))
        val state = Output(State())    
    })
    val state = RegInit(rest) //have a state reg to make things clocked

    io.state := state
    val out_int = 2.U   

    switch (state) {
      is (rest){
        //do reset things
        when(false.B){
            //do condition_one things
            //set state accordingly
        }.elsewhen(true.B){
            //do condition_two things
            //set state accordingly
        }.otherwise{
            //do state -> state operation
            //can set state but don't have to
        }
      }  
      is (init) {
        //and repeat so on so forth
      }
      is (run) {}
      is (done) {}
    }
 
    io.out_val := out_int //use some internal value and wire it to the output at the end
}

object NotMain extends App {
    //val path = System.getProperty("user.dir") + "/source/load-ivy.sc"
    //interp.load.module(ammonite.ops.Path(java.nio.file.FileSystems.getDefault().getPath(path)))
    println(ChiselStage.emitSystemVerilog(
        gen = new FSM(4),
        firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info")
        ))
}