// See README.md for license details.

package qlib

import chisel3._
import circt.stage._
import chisel3.experimental._
import chisel3.stage._
import java.io._
//import firrtl.options.BareShell

//RawModule


/**
 * oracle OR in1,in2,out1 { "or.v" }
 * 
 * or.v
 * module top ( a, b, c );
 *  input a,b;
 *  output c;
 *  assign c = a | b;
 * endmodule
 */

import scala.sys.process._
import scala.io.Source

object QasmWriter{
  def emitQASMFile(
    gen:         => RawModule,
    mod_name:      String,
  ): Int = {
    ChiselStage.emitSystemVerilogFile(
      gen, firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info", "--verilog", "-o", mod_name + ".v")
    ) //create the verilog file

    if(!combinationalCheck(mod_name)){ //do rule checking
      println("Failed. Ensure your circuit is combinational and does not generated clocked logic.")
      return 1
    }

    if(!multibitArgumentCheck(mod_name)){
      println("Failed. Ensure inputs and outputs are one bit wide.")
      return 1
    }
    
    var io_ports = Seq("bash", "-c", "grep -oP '\\Kio_in_.*(?=,)' " ++ mod_name ++ ".v | tr '\\n' ','").!!.trim // get inputs
    io_ports = io_ports ++ Seq("bash", "-c", "grep -oP 'output \\K.*' " ++ mod_name ++ ".v | tr '\\n' ',' | sed 's/,$//'").!!.trim // get outputs, strip trailing comma

    val boilerfile = new File(mod_name ++ "temp.qasm") //make a temp QASM file and add boilerplate
    val bw = new BufferedWriter(new FileWriter(boilerfile))
    bw.write("OPENQASM 2.0;\n include \"qelib1.inc\";\n\n")
    bw.write("oracle O_" ++ mod_name ++ " " ++ io_ports + " { \"" ++ mod_name ++ "temp.v\" }") // should add all required inputs + outputs
    bw.close()

    var verilog_file= Array[String]()
    for (line <- Source.fromFile(mod_name++".v").getLines()){
      print(line)
      if (line == 1) verilog_file :+= "module " ++ mod_name ++ "(" ++ io_ports ++ ");"
      else if (line == 2) verilog_file :+= ""
      else if (line == 3)verilog_file :+= ""
      else verilog_file :+= line
    }

    // var verilog_file: Array[String] = (os.read.lines(os.pwd/(mod_name ++ ".v"))).toArray
    // verilog_file(1) = "module " ++ mod_name ++ "(" ++ io_ports ++ ");"
    // verilog_file(2) = ""
    // verilog_file(3) = ""

    val format_verilog = new File(mod_name ++ "temp.v")
    val veriwriter = new BufferedWriter(new FileWriter(format_verilog))
    for(i <- 0 until verilog_file.length){
      print(verilog_file(i))
      veriwriter.write(verilog_file(i))
    }
    veriwriter.close()

    val qasmfile = new File(mod_name ++ ".qasm")
    val qasmwriter = new BufferedWriter(new FileWriter(qasmfile))
    qasmwriter.write(("staq_oracle_synthesizer < " ++ boilerfile.getName()).!!)//write output of Staq synth into output file
    qasmwriter.close()

    ("rm " ++ mod_name ++ "temp.qasm").! //delete temp file

    return 0
  } 

  def combinationalCheck(name: => String): Boolean = {
    if(Seq("bash", "-c", "grep -oP '\\Kreg' " ++ name ++ ".v | tr '\\n' ','").!!.trim.isEmpty()){ // get inputsSeq("bash", "grep -oP 'reg' " ++ name ++ ".v").!!.trim.isEmpty()){ //if reg is present in verilog, fail
      return true
    }
    return false
  }

  def multibitArgumentCheck(name: => String): Boolean = {
    if(Seq("bash", "-c", "grep -oP '\\K\\[' " ++ name ++ ".v | tr '\\n' ','").!!.trim.isEmpty()){
      return true
    }
    return false
  }
}

abstract class QModule extends Module {
  def toQuantum = true

  def emitQASMFile(
    gen:         => RawModule,
    //args:        Array[String] = Array.empty,
    //firtoolOpts: Array[String] = Array.empty
  ) = {
    ChiselStage.emitSystemVerilogFile(
      gen, firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info", "--verilog", "-o", gen.desiredName ++ ".v")
    ) //create the verilog file

    

    var io_ports = "grep -oP '\\Kio_in_.*(?=,)' Parity.v | tr '\\n' ','".!!.trim // get inputs
    io_ports = io_ports ++ "grep -oP 'output \\K.*' Parity.v | tr '\\n' ',' | sed 's/,$//'".!!.trim // get outputs, strip trailing ,


    val boilerfile = new File(gen.name ++ "temp.qasm") //make a QASM file and add boilerplate
    val bw = new BufferedWriter(new FileWriter(boilerfile))
    bw.write("'OPENQASM 2.0;\n include \"qelib1.inc\";\n\n")
    bw.write("oracle " ++ gen.name ++ " " ++ io_ports + " { \"" ++ gen.desiredName ++ ".v \" }") // should add all required inputs + outputs
    bw.close()


    val qasmfile = new File(gen.name ++ ".qasm")
    val qasmwriter = new BufferedWriter(new FileWriter(qasmfile))
    qasmwriter.write(("staq_oracle_synthesizer < " ++ boilerfile.getName()).!!)
    qasmwriter.close()

    ("rm " ++ gen.name ++ "temp.qasm").!
  }

  
}


