package tester

import chisel3._
import circt.stage.ChiselStage
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import chisel3.experimental.BundleLiterals._


import FIR._
import FSM._
import FFT._
import Hamming_Syndrome._

import breeze.signal._
import breeze.math.{Complex => breeze_Complex}
import breeze.linalg.DenseVector

import mySwFFT._
import breeze.numerics.log
import chisel3.util.Log2
import breeze.numerics.log2


class DoTests extends AnyFlatSpec with ChiselScalatestTester {
    behavior.of ("FIR")
    

    it should "test FIR filter" in {
    val factors = Seq(5, 6, 7, 3)
    val factorsChi = factors.map(_.S)

    println(ChiselStage.emitSystemVerilog(
    gen = new FIR(12, 12, factorsChi),
    firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info")
    ))
    //println(chiselX)
    //println(factors)
    test(new FIR(12, 12, factorsChi)) {dut =>

        
        val testX = (0 until 10)
        val chiselX = testX.map(_.asSInt)
        
        dut.io.in.poke(chiselX(0))
        dut.clock.step(1)
        dut.io.in.poke(chiselX(1))
        dut.clock.step(1)
        dut.io.in.poke(chiselX(2))
        dut.clock.step(1)
        for (i <- 3 until 9){
            dut.io.in.poke(chiselX(i))
            dut.io.out.expect(testX(i)*factors(0) + testX(i-1)*factors(1) + testX(i-2)*factors(2) + testX(i-3)*factors(3))
            dut.clock.step(1)

        }
    }
}
    it should "output a verilog file" in {
        val factors = Seq(5, 6, 7, 3)
        val factorsChi = factors.map(_.S)
        
        ChiselStage.emitSystemVerilogFile(
            gen = new FIR(12, 12, factorsChi),
            firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info", "-o", "FIR.sv")
        )

        test(new FIR(12, 12, factorsChi){})
    }

    behavior.of("FTT")

    it should "test accuracy of FFT" in {
        val log2N = 4
        val in_w = 32
        val out_w = 32
        var waveform = Vec(1 << log2N, new Complex(SInt(in_w.W), SInt(in_w.W)))
        var sw_array = Array[breeze_Complex]()        

        for (i <- 0 until 1 << log2N){ //this should put in a cos wave
            waveform(i).real = (math.sin(2*math.Pi/7 * i)*(1<<30)).round.toInt.S(in_w.W)
            waveform(i).im = 0.S(in_w.W)
            sw_array = sw_array :+ new breeze_Complex(math.sin(2*math.Pi/7 * i), 0)
        }

        val sw_fft_val = mySwFFT.SW_FFT_wrapper.SW_FFT(sw_array) // SW implementation of target algorithm

        for (i <- 0 until (1 << log2N)){
            println(sw_fft_val(i))
        }

        test(new FFT(in_w, out_w, log2N)) { dut =>
            dut.io.in.zip(waveform).foreach { case (io, input) =>
                io.real.poke(input.real)
                io.im.poke(input.im)
            }
            dut.clock.step(1)
            dut.io.out.zip(sw_fft_val).foreach { case (io, output) =>
                if((io.real.peekInt() - ((output.real*(1<<(in_w-2))).round.toInt).abs > (log2N/2))) io.real.expect((output.real*(1<<(in_w-2))).round.toInt.S)
                if((io.im.peekInt() - ((output.imag*(1<<(in_w-2))).round.toInt).abs > (log2N/2))) io.im.expect((output.imag*(1<<(in_w-2))).round.toInt.S)
            }
        }
    }

    // behavior.of("Parity")

    // it should "build parity checker hardware" in {
    //     println(ChiselStage.emitSystemVerilog(
    //     gen = new Parity(8),
    //     firtoolOpts = Array("-disable-all-randomization", "-strip-debug-info")
    //     ))
    //     test(new Parity(8){})
    // }
}
